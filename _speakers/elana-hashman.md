---
name: Elana Hashman
talks:
- Understanding The Black Magic of Python Wheels
---

Elana Hashman is a Python Packaging Authority committer, hacking on Python wheels and the manylinux project, and a Debian Developer, uploading, where she maintains the packaged Clojure ecosystem in Debian and Ubuntu. She currently works as a Reliability Engineer at Two Sigma, where she wrangles their Kubernetes cluster and maintains their internal deployments system.