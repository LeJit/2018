---
name: Sumana Harihareswara
talks:
- "Python Grab Bag: A Set of Short Plays"
---
Sumana Harihareswara is an open source contributor and leader who has
contributed to the Python Package Index, GNOME, MediaWiki, Zulip, GNU
Mailman, and other FLOSS projects. She has keynoted Open Source Bridge and
other open source conventions, spoken at PyCon and OSCON ([past
talks](https://www.harihareswara.net/talks.html)), and manages and maintains
open source projects as [Changeset Consulting](http://changeset.nyc/). She’s
also a stand-up comedian who’s performed at Worldcon, WisCon, and AlterConf,
and who serves as the charity auctioneer for the Tiptree Award. She is an
alumna of [the Recurse Center](https://www.recurse.com/).

Sumana and [Jason](/speakers/jason-owen/) presented the play [Code Review, Forwards and
Back](https://2017.pygotham.org/talks/code-review-forwards-and-back/) at
PyGotham 2017.
