---
name: Roy Williams
talks:
- A Practical Guide to Adopting Python Types
---

Roy is the Tech Lead of the Developer Experience team at Lyft, where he focuses on measuring and improving developer productivity.  Roy has been passionate about enabling engineers to be more productive and ship code faster for his entire career.  Previous to Lyft, Roy was an Engineering Manager at Facebook on the Continuous Integration team, working at Google and Microsoft before that.  Roy graduated with a BSc. in Computer Science from Duke University in 2005.  He's a contributor to mypy, pyannotate, pyannotate-pytest, and pylint.

Roy lives in Englewood Cliffs New Jersey with his wife Tara and two young boys Hudson and Miles.