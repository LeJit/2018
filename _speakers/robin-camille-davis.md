---
name: Robin Camille Davis
talks:
- Keep it secret, keep it safe! Preserving anonymity by subverting stylometry
---

Robin Camille Davis is the Emerging Technologies & Online Learning Librarian at John Jay College of Criminal Justice (CUNY), where she leads web projects in the library, supports information literacy instruction, and researches authorship attribution. She has given presentations internationally at library and humanities conferences and is active on Twitter as @robincamille.